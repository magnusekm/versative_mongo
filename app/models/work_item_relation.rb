class WorkItemRelation
  include Mongoid::Document
  
  field :name, type: String
  
  belongs_to    :incoming, :class_name => 'WorkItem'
  belongs_to    :outgoing, :class_name => 'WorkItem'
  
end