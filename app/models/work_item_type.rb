class WorkItemType
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :name, type: String
  
  has_and_belongs_to_many :custom_fields
  has_many :work_items

  validates_presence_of   :name
  
  def to_s
    name
  end
  
end
