class Document
  include Mongoid::Document
  
  field :name, type: String
  key :name do
    Document.count + 1
  end
  
  has_and_belongs_to_many :work_items

end
