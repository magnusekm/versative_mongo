class Comment
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Versioning

  field :text, type: String

  embedded_in :work_item
  
  after_save :print_message
  
  def print_message
    p "Comment was created"
  end

end
