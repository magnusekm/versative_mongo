class User < ActiveRecord::Base
  include Tenacity

  t_has_many :work_items
  
  validates_presence_of :name
  validates_uniqueness_of :name
  
  def to_s
    name
  end
end