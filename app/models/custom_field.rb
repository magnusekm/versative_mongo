class CustomField
  TYPES = %w( text date number option )

  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :name,    type: String
  field :type,    type: String, default: 'text'
  field :values,  type: Array

  validates_inclusion_of  :type,  :in => TYPES
  validates_presence_of   :name
  
  has_and_belongs_to_many :work_item_type

  def values=(values)
    write_attribute :values, values.split
  end
end
