class WorkItem
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Versioning
  include Mongoid::Tree
  include Tenacity
  include Tire::Model::Search
  include Tire::Model::Callbacks
  
  field :name, type: String
  
  t_belongs_to  :user
  belongs_to    :work_item_type
  belongs_to    :project
  embeds_many   :comments
  has_many      :outgoings, :class_name => 'WorkItemRelation', :foreign_key => 'incoming_id'
  has_many      :incomings, :class_name => 'WorkItemRelation', :foreign_key => 'outgoing_id'
  has_and_belongs_to_many :documents
  
  validates_presence_of   :work_item_type

  shard_key :project_id

end
