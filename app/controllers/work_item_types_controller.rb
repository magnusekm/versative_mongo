class WorkItemTypesController < ApplicationController
  # GET /work_item_types
  # GET /work_item_types.json
  def index
    @work_item_types = WorkItemType.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @work_item_types }
    end
  end

  # GET /work_item_types/1
  # GET /work_item_types/1.json
  def show
    @work_item_type = WorkItemType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @work_item_type }
    end
  end

  # GET /work_item_types/new
  # GET /work_item_types/new.json
  def new
    @work_item_type = WorkItemType.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @work_item_type }
    end
  end

  # GET /work_item_types/1/edit
  def edit
    @work_item_type = WorkItemType.find(params[:id])
  end

  # POST /work_item_types
  # POST /work_item_types.json
  def create
    @work_item_type = WorkItemType.new(params[:work_item_type])

    respond_to do |format|
      if @work_item_type.save
        format.html { redirect_to @work_item_type, notice: 'Work item type was successfully created.' }
        format.json { render json: @work_item_type, status: :created, location: @work_item_type }
      else
        format.html { render action: "new" }
        format.json { render json: @work_item_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /work_item_types/1
  # PUT /work_item_types/1.json
  def update
    @work_item_type = WorkItemType.find(params[:id])

    respond_to do |format|
      if @work_item_type.update_attributes(params[:work_item_type])
        format.html { redirect_to @work_item_type, notice: 'Work item type was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @work_item_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /work_item_types/1
  # DELETE /work_item_types/1.json
  def destroy
    @work_item_type = WorkItemType.find(params[:id])
    @work_item_type.destroy

    respond_to do |format|
      format.html { redirect_to work_item_types_url }
      format.json { head :ok }
    end
  end
end
