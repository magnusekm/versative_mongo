class CommentsController < ApplicationController
  before_filter :find_work_item
  # GET /comments
  # GET /comments.json
  def index
    @comments = @work_item.comments

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @comments }
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    @comment = @work_item.comments.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @comment }
    end
  end

  # GET /comments/new
  # GET /comments/new.json
  def new
    @comment = @work_item.comments.build

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @comment }
    end
  end

  # GET /comments/1/edit
  def edit
    @comment = @work_item.comments.find(params[:id])
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment = @work_item.comments.build(params[:comment])

    respond_to do |format|
      if @comment.save
        format.html { redirect_to work_item_comment_url(@work_item,@comment), notice: 'Comment was successfully created.' }
        format.json { render json: @comment, status: :created, location: @comment }
      else
        format.html { render action: "new" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /comments/1
  # PUT /comments/1.json
  def update
    @comment = @work_item.comments.find(params[:id])

    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to work_item_comment_url(@work_item,@comment), notice: 'Comment was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment = @work_item.comments.find(params[:id])
    @comment.destroy

    respond_to do |format|
      format.html { redirect_to work_item_comments_url(@work_item) }
      format.json { head :ok }
    end
  end
  
  def find_work_item
    @work_item = WorkItem.find(params[:work_item_id])
  end
end
