# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :work_item do
    sequence(:name) {|n| "Work item #{n}" }
    work_item_type
  end
end