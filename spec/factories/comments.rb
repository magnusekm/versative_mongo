# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :comment do
    sequence(:text) {|n| "Comment #{n}" }
  end
end