# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :work_item_type do
    name "Requirement"
  end
end