# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :custom_field do
    name "price"
    type "number"
  end
end