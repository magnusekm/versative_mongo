require 'spec_helper'

describe WorkItem do
  before do
    Tire.index('work_items').delete
    DatabaseCleaner.clean
    
    price = FactoryGirl.create :custom_field, name: 'price',  type: 'number'
    size  = FactoryGirl.create :custom_field, name: 'size',   type: 'text'

    task        = FactoryGirl.create :work_item_type, name: 'task',         custom_fields: [price]
    requirement = FactoryGirl.create :work_item_type, name: 'requirement',  custom_fields: [size]

    @task         = FactoryGirl.build :work_item, work_item_type: task
    @requirement  = FactoryGirl.build :work_item, work_item_type: requirement
    @parent       = FactoryGirl.build :work_item, work_item_type: requirement, children: [@requirement]
  end
  
  it "should be valid" do
    @task.should be_valid
    @requirement.should be_valid
  end
  
  it "should be possible set custom attributes" do
    @task[:price] = 15
    @task.should be_valid
    @requirement[:size] = 'medium'
    @requirement.should be_valid
  end
  
  describe "tire search" do
    before do
      @task[:price] = 15
      @requirement[:size] = 'medium'
      @task.save
      @requirement.save
      @parent.save
      Tire.index('work_items').refresh
    end
    
    it "find all work items" do
      WorkItem.tire.search("Work item").results.size.should eql 3
    end
    
    it "find task" do
      WorkItem.tire.search("15", :load => true).results.first.should eql @task
    end
    
    it "find task" do
      WorkItem.tire.search("price:15", :load => true).results.first.should eql @task
    end
    
    it "find requirement" do
      WorkItem.tire.search("medium", :load => true).results.first.should eql @requirement
    end
    
    it "find requirement as child" do
      parent = @parent
      s = Tire.search 'work_items', :load => true do
        # query do
        #   string "parent_id:#{parent.id}"
        # end
        # filter :term, :parent_id => parent.id
        filter :or, { :term =>  { :parent_id => parent.id } },
                    { :range => { :price => {:from => 5, :to => 50 } } }
      end
      s.results.size.should eql 2
    end
  end
  
  describe "tire search by child" do
    before do
      @requirement.comments.create(:text => 'Comment text')
      Tire.index('work_items').refresh
    end
    
    it "find parent" do
      WorkItem.tire.search("Comment", :load => true).results.first.should eql @requirement
    end
  end
end
